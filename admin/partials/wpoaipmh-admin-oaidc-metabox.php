<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/admin/partials
 */

$slug = "_wpoaipmh";

$_record_oai_activated = get_post_meta( $post_id, $slug . '_record_oai_activated', true );
$_record_title = get_post_meta( $post_id, $slug . '_record_title', true );
$_record_description = get_post_meta( $post_id, $slug . '_record_description', true );
$_record_year_original_publication = get_post_meta( $post_id, $slug . '_record_year_original_publication', true );
$_record_work_languages = json_decode( get_post_meta( $post_id, $slug . '_record_work_languages', true ), true );
$_record_technology = get_post_meta( $post_id, $slug . '_record_technology', true );
$_record_publication_types = json_decode( get_post_meta( $post_id, $slug . '_record_publication_types', true ), true );
$_record_complementary_publication_types = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_publication_types', true ), true );
$_record_procedural_modalities = json_decode( get_post_meta( $post_id, $slug . '_record_procedural_modalities', true ), true );
$_record_complementary_procedural_modalities = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_procedural_modalities', true ), true );
$_record_mechanisms = json_decode( get_post_meta( $post_id, $slug . '_record_mechanisms', true ), true );
$_record_complementary_mechanisms = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_mechanisms', true ), true );
$_record_formats = json_decode( get_post_meta( $post_id, $slug . '_record_formats', true ), true );
$_record_complementary_formats = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_formats', true ), true );
$_record_literary_qualities = json_decode( get_post_meta( $post_id, $slug . '_record_literary_qualities', true ), true );
$_record_creators = json_decode( get_post_meta( $post_id, $slug . '_creators', true ), true );
$_record_publishers = json_decode( get_post_meta( $post_id, $slug . '_publishers', true ), true );
$_record_source_name = get_post_meta( $post_id, $slug . '_record_source_name', true );
$_record_source_authors = get_post_meta( $post_id, $slug . '_record_source_authors', true );


$metadata = [
"formats" => ["Audio", "Database", "Image / Picture", "Physical Artefact", "Search Engine", "Text", "Video", "Video Game", "Virtual Environment", "Other"],
"mechanisms" => ["Biomonitoring Device", "Camera", "Controller", "Device ID", "Display", "Locational Device", "Keyboard", "Microphone", "Midi Controller", "Motion Capture", "Printout", "Radio", "Screen", "Smartphone", "Speaker", "Tablet", "Touchscreen", "Vibration", "Other"],
"procedural_modalities" => ["Activation - Deactivation", "Alteration", "Detection", "Download", "Generation", "Insertion", "Login", "Manipulation", "Navigation", "HyperTextual Navigation", "Graphical Navigation", "Spatial Navigation", "Temporal Navigation", "Scheduled Navigation", "Networked Interactivity", "Observation", "Selection", "Transcoding", "Upload", "Other"],
"publication_types" => ["Application - Computer Program", "Audio Tape", "CD-ROM", "DVD-ROM", "Exhibition", "File", "Film", "Floppy Disk", "Installation", "Site-Specific Installation", "Locative Installation", "Permanent Installation", "Online Gallery", "Performance", "Single Performance", "Repeatable Performance", "Locative Performance", "Site-Specific Performance", "Platform", "Photographic Print", "Print", "Social Network", "USB Drive", "VHS", "Vynil Record", "Virtual World", "Website", "Other"]
];

?>

<style type="text/css">
	.wpoaipmh_oai_dc_group {
		margin: 5px 0;
		padding:5px;
	}
	.wpoaipmh_oai_dc_group h2 {
		padding: 0 !important;
	}
	.wpoaipmh_oai_dc_group .addRowBtn {
		background: #333;
		color: white;
		padding: 5px;
		text-align: center;
		width:auto;
		border-radius: 2px;
		cursor: pointer;
	}
	.form-control {
		display: block;
		width: 300px;
	}
</style>

<?php wp_nonce_field( 'wpoaipmh_oai_dc_metabox', 'wpoaipmh_oai_dc_metabox_nonce' ); ?>

<div class="wpoaipmh_oai_dc_group">
	<h3>Options</h3>
	<table class="form-table">
		<tr>
			<th><label for="<?= $slug ?>_record_oai_activated">Show on OAI-PMH</label></th>
			<td><input type="checkbox" name="<?= $slug ?>_record_oai_activated" id="_record_oai_activated" <?php if($_record_oai_activated == "on") { echo "checked"; } ?> ></td>
		</tr>
	</table>
</div>

<div id="wpoaipmh_form">
	<div class="wpoaipmh_oai_dc_group">
		<h3>Record</h3>
		<table class="form-table">
			<tr>
				<th><label for="<?= $slug ?>_record_title">Title: </label></th>
				<td><input type="text" name="<?= $slug ?>_record_title" value="<?= $_record_title ?>"></td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_description">Description: </label></th>
				<td><textarea name="<?= $slug ?>_record_description" cols="30" rows="10"><?= $_record_description ?></textarea></td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_year_original_publication">Year of Publication: </label></th>
				<td><input type="number" min="1500" name="<?= $slug ?>_record_year_original_publication" value="<?= $_record_year_original_publication ?>"></td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_work_languages">Work Languages: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_work_languages[]">
						<?php 
							foreach ($_record_work_languages as $work_language) {
								echo '<option selected="selected">' . $work_language . '</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_technology">Technology: </label></th>
				<td><input type="text" name="<?= $slug ?>_record_technology" value="<?= $_record_technology ?>"></td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_publication_types">Publication Types: </label></th>
				<td>
					<select class="form-control" multiple="multiple" name="<?= $slug ?>_record_publication_types[]" multiple>
						<?php 
							foreach ( $metadata["publication_types"] as $pubtype ) { 
								if(in_array($pubtype, $_record_publication_types)) {
									echo '<option selected="selected">' . $pubtype . '</option>';
								}
								else {
									echo '<option>' . $pubtype . '</option>';
								}
						 	} 
					 	?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_complementary_publication_types">Complementary Publication Types: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_complementary_publication_types[]">
						<?php 
							foreach ($_record_complementary_publication_types as $publication_type) {
								echo '<option selected="selected">' . $publication_type . '</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_procedural_modalities">Procedural Modalities: </label></th>
				<td>
					<select class="form-control" multiple="multiple" name="<?= $slug ?>_record_procedural_modalities[]">
						<?php 
							foreach ( $metadata["procedural_modalities"] as $procmodality ) {
								if( in_array($procmodality, $_record_procedural_modalities)) {
									echo '<option selected="selected">' . $procmodality . '</option>';
								}
								else {
									echo '<option>' . $procmodality . '</option>';
								}
							} 
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_complementary_procedural_modalities">Complementary Procedural Modalities: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_complementary_procedural_modalities[]">
						<?php 
							foreach ($_record_complementary_procedural_modalities as $procedural_modality) {
								echo '<option selected="selected">' . $procedural_modality . '</option>';
							}
						?>	
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_mechanisms">Mechanisms: </label></th>
				<td>
					<select class="form-control" multiple="multiple" name="<?= $slug ?>_record_mechanisms[]">
						<?php foreach ( $metadata["mechanisms"] as $mechanism ) { ?>
							<option value="<?= $mechanism ?>"><?= $mechanism ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_complementary_mechanisms">Complementary Mechanisms: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_complementary_mechanisms[]">
						<?php 
							foreach ($_record_complementary_mechanisms as $mechanism) {
								echo '<option selected="selected">' . $mechanism . '</option>';
							}
						?>	
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_formats">Formats: </label></th>
				<td>
					<select class="form-control" multiple="multiple" name="<?= $slug ?>_record_formats[]" multiple>
						<?php foreach ( $metadata["formats"] as $format ) { ?>
							<option value="<?= $format ?>"><?= $format ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_complementary_formats">Complementary Formats: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_complementary_formats[]">
						<?php 
							foreach ($_record_complementary_formats as $format) {
								echo '<option selected="selected">' . $format . '</option>';
							}
						?>	
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_literary_qualities">Literary Qualities: </label></th>
				<td>
					<select class="form-control tag-input" multiple="multiple" name="<?= $slug ?>_record_literary_qualities[]">
						<?php 
							foreach ($_record_literary_qualities as $literary_quality) {
								echo '<option selected="selected">' . $literary_quality . '</option>';
							}
						?>	
					</select>
				</td>
			</tr>
		</table>
	</div>

	<div class="wpoaipmh_oai_dc_group">
		<h3>Creator</h3>
		<table class="form-table">
			<tr class="duplicatable">
				<th><label for="<?= $slug ?>_creator_name">Name: </label></th>
				<td><input type="text" name="<?= $slug ?>_creator_name[]"></td>
				<th><label for="<?= $slug ?>_creator_role">Role: </label></th>
				<td><input type="text" name="<?= $slug ?>_creator_role[]"></td>
				<th><label for="<?= $slug ?>_creator_uri">URI: </label></th>
				<td><input type="text" name="<?= $slug ?>_creator_uri[]"></td>
			</tr>
			<?php
            if ($_record_creators) {
			foreach ($_record_creators as $creator) {
				echo '
					<tr>
						<th><label for="' . $slug . '_creator_name">Name: </label></th>
						<td><input type="text" name="' .  $slug . '_creator_name[]" value="' . $creator["name"] . '"></td>
						<th><label for="' .  $slug . '_creator_role">Role: </label></th>
						<td><input type="text" name="' .  $slug . '_creator_role[]" value="' . $creator["role"] . '"></td>
						<th><label for="' .  $slug . '_creator_uri">URI: </label></th>
						<td><input type="text" name="' .  $slug . '_creator_uri[]" value="' . $creator["uri"] . '"></td>
					</tr>
				';
			}
            }
			?>
			
		</table>
		<div class="addRowBtn">Add</div>
	</div>

	<div class="wpoaipmh_oai_dc_group">
		<h3>Publisher</h3>
		<table class="form-table">
			<tr class="duplicatable">
				<th><label for="<?= $slug ?>_publisher_name">Name: </label></th>
				<td><input type="text" name="<?= $slug ?>_publisher_name[]"></td>
				<th><label for="<?= $slug ?>_publisher_uri">URI: </label></th>
				<td><input type="text" name="<?= $slug ?>_publisher_uri[]"></td>
			</tr>
			<?php
            if($_record_publishers) {
			foreach ($_record_publishers as $publisher) {
				echo '
					<tr>
						<th><label for="' . $slug . '_publisher_name">Name: </label></th>
						<td><input type="text" name="' . $slug . '_publisher_name[]" value="' . $publisher["name"] .'"></td>
						<th><label for="' . $slug . '_publisher_uri">URI: </label></th>
						<td><input type="text" name="' . $slug . '_publisher_uri[]" value="' . $publisher["uri"] .'"></td>
					</tr>
				';
			}
            }
			?>
		</table>
		<div class="addRowBtn">Add</div>
	</div>

	<div class="wpoaipmh_oai_dc_group">
		<h3>Source Entry</h3>
		<table class="form-table">
			<tr>
				<th><label for="<?= $slug ?>_record_source_name">Name: </label></th>
				<td><input type="text" name="<?= $slug ?>_record_source_name" value="<?= $_record_source_name ?>"></td>
			</tr>
			<tr>
				<th><label for="<?= $slug ?>_record_source_authors">Authors: </label></th>
				<td><input type="text" name="<?= $slug ?>_record_source_authors" value="<?= $_record_source_authors ?>"></td>
			</tr>
		</table>
	</div>

</div>

<script type="text/javascript">

function oai_activated_listener() {
	if(document.getElementById("_record_oai_activated").checked == true) {
		document.getElementById("wpoaipmh_form").style.display = "block";

	}
	else {
		document.getElementById("wpoaipmh_form").style.display = "none";		
	}
}

document.onready = function() {

	oai_activated_listener();

	document.getElementById("_record_oai_activated").onchange = oai_activated_listener;

	var addRowButtons = document.querySelectorAll(".addRowBtn");
	for(var i = 0; i < addRowButtons.length; i++) {
		addRowButtons[i].onclick = function(e) {
			var table = e.target.parentNode.querySelector('.form-table');
			var rowclone = e.target.parentNode.querySelector('.duplicatable').cloneNode(true);
			table.appendChild(rowclone);
		}
	}

	$(".tag-input").select2({
	    tags: true,
	    placeholder: "Select One or More",
	    multiple: true,
	    dropdownAutoWidth: true,
	    tokenSeparators: [',']
	});

};

</script>




