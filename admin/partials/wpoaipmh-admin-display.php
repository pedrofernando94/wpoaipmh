<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h1><?= esc_html(get_admin_page_title()); ?></h1>
    <form action="options.php" method="post">
        <?php
        // output security fields for the registered setting "oai-pmh-repository-options"
        settings_fields('wpoaipmh_options');

        // output setting sections and their fields
        // (sections are registered for "wpoaipmh", each field is registered to a specific section)
        do_settings_sections('wpoaipmh');

        // output save settings button
        submit_button('Save Settings');
        ?>
    </form>
</div>