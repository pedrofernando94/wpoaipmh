<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/admin
 * @author     Pedro Monteiro <29683@ufp.edu.pt>
 */
class Wpoaipmh_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wpoaipmh-admin.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wpoaipmh-admin.js', array( 'jquery' ), $this->version, false );
        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/select2.full.min.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Add Menu for Options inside Admin Panel.
     *
     * @since 1.0.0
     */
    public function options_page() {

        add_menu_page(
            'Wordpress OAI-PMH',
            'Wordpress OAI-PMH',
            'manage_options',
            plugin_dir_path(__FILE__) . 'partials/wpoaipmh-admin-display.php',
            null,
            null,
            null
        );

    }

    /**
     * Add Custom Taxonomies for OAI Functionality. This may be necessary for OAI Sets implementation.
     *
     * @since 1.0.0
     */
    // public function register_custom_taxonomies() {

    //     register_taxonomy(
    //         'set',
    //         'post',
    //         array(
    //             'label' => __( 'OAI Sets' ),
    //             'rewrite' => false,
    //             'hierarchical' => false,
    //         )
    //     );

    // }

    /**
     * Add Custom Metadata Box for Open Archives Initiative Dublin Core...
     *
     * @since 1.0.0
     */
    public function register_wpoaipmh_metabox() {

        add_meta_box(
            'wpoaipmh_oai_dc_metabox',
            'OAI-DC',
            array( $this, 'wpoaipmh_metabox_callback' ),
            'post'
        );

    }

    /**
     * Used by register_oai_dc_metabox()->add_metadata_box() to implement the fields as a html form.
     *
     * @since 1.0.0
     */
    public function wpoaipmh_metabox_callback($post) {

        $post_id = $post->ID;
        include 'partials/wpoaipmh-admin-oaidc-metabox.php';

    }

    /**
     * Saves the fields added for metadata in the post whenever a post is saved.
     *
     * @since 1.0.0
     */
    public function wpoaipmh_metabox_save($post) {
        //TODO: Check if user can edit post
        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['wpoaipmh_oai_dc_metabox_nonce'] ) ||
            !wp_verify_nonce( $_POST['wpoaipmh_oai_dc_metabox_nonce'], "wpoaipmh_oai_dc_metabox" ) ) {
            return $post;
        }

        /* Filter the _POST array and only take in the inputs relative to wpoaipmh metabox... */
        $wpoaipmh_oai_dc_metabox_fields = array_filter($_POST, function($key) {
            return (substr($key, 0, 9) == "_wpoaipmh");
        }, ARRAY_FILTER_USE_KEY);

        //var_dump($wpoaipmh_oai_dc_metabox_fields);
        //exit();

        $_wpoaipmh_creators = array( );
        for ( $i=0; $i < count($_POST["_wpoaipmh_creator_name"]); $i++ ) { 
            $creator = array();
            $creator["name"] = $_POST["_wpoaipmh_creator_name"][$i];
            $creator["role"] = $_POST["_wpoaipmh_creator_role"][$i];
            $creator["uri"] = $_POST["_wpoaipmh_creator_uri"][$i];
            if(!empty($creator["name"]) || !empty($creator["role"]) || !empty($creator["uri"])) {
                array_push( $_wpoaipmh_creators, $creator );
            }
        }

        $_wpoaipmh_publishers = array( );
        for ( $i=0; $i < count($_POST["_wpoaipmh_publisher_name"]); $i++ ) { 
            $publisher = array();
            $publisher["name"] = $_POST["_wpoaipmh_publisher_name"][$i];
            $publisher["uri"] = $_POST["_wpoaipmh_publisher_uri"][$i];
            if(!empty($publisher["name"]) || !empty($publisher["role"])) {
                array_push($_wpoaipmh_publishers, $publisher);
            }
        }

        $wpoaipmh_oai_dc_metabox_fields = [
        "_wpoaipmh_record_oai_activated"        =>  !isset($_POST["_wpoaipmh_record_oai_activated"]) ? "off" : "on",
        "_wpoaipmh_record_title"                =>  $_POST["_wpoaipmh_record_title"],
        "_wpoaipmh_record_description"          =>  $_POST["_wpoaipmh_record_description"],
        "_wpoaipmh_record_year_original_publication" => $_POST["_wpoaipmh_record_year_original_publication"],
        "_wpoaipmh_record_work_languages"       =>  json_encode($_POST["_wpoaipmh_record_work_languages"]),
        "_wpoaipmh_record_technology"           =>  $_POST["_wpoaipmh_record_technology"],
        "_wpoaipmh_record_publication_types"    =>  json_encode($_POST["_wpoaipmh_record_publication_types"]),
        "_wpoaipmh_record_complementary_publication_types" =>   json_encode($_POST["_wpoaipmh_record_complementary_publication_types"]),
        "_wpoaipmh_record_procedural_modalities"        =>  json_encode($_POST["_wpoaipmh_record_procedural_modalities"]),
        "_wpoaipmh_record_complementary_procedural_modalities"  =>  json_encode($_POST["_wpoaipmh_record_complementary_procedural_modalities"]),
        "_wpoaipmh_record_mechanisms"   =>  json_encode($_POST["_wpoaipmh_record_mechanisms"]),
        "_wpoaipmh_record_complementary_mechanisms"     =>  json_encode($_POST["_wpoaipmh_record_complementary_mechanisms"]),
        "_wpoaipmh_record_formats"      =>  json_encode($_POST["_wpoaipmh_record_formats"]),
        "_wpoaipmh_record_complementary_formats"        =>  json_encode($_POST["_wpoaipmh_record_complementary_formats"]),
        "_wpoaipmh_record_literary_qualities"       =>  json_encode($_POST["_wpoaipmh_record_literary_qualities"]),
        "_wpoaipmh_record_source_name"      => $_POST["_wpoaipmh_record_source_name"],
        "_wpoaipmh_record_source_authors"   => $_POST["_wpoaipmh_record_source_authors"],
        "_wpoaipmh_creators"        => json_encode($_wpoaipmh_creators),
        "_wpoaipmh_publishers"      => json_encode($_wpoaipmh_publishers),
        ];

        foreach( $wpoaipmh_oai_dc_metabox_fields as $field => $value ) {
            /* TODO: Get the posted data and sanitize it for use in XML. Should implement some validation too! */
            //$new_meta_value = $_POST[$field];
            $new_meta_value = $value;

            /* Get the meta key. */
            $meta_key = $field;
            /* Get the meta value of the custom field key. */
            $meta_value = get_post_meta( $post, $meta_key, true );
            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && '' == $meta_value )
                add_post_meta( $post, $meta_key, $new_meta_value, true );

            /* If the new meta value does not match the old value, update it. */
            elseif ( $new_meta_value && $new_meta_value != $meta_value )
                update_post_meta( $post, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            elseif ( '' == $new_meta_value && $meta_value )
                delete_post_meta( $post, $meta_key, $meta_value );
        }

    }

}
