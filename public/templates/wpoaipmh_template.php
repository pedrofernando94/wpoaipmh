<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/public/partials
 */

use WPOAIPMH\Repository;
$response = new WPOAIPMH\Repository\ResponseBuilder();
$response->render();
