<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/public
 * @author     Pedro Monteiro <29683@ufp.edu.pt>
 */
class Wpoaipmh_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    /**
     * Register a Rewrite Rule that matches this url pattern: /oai?request...
     * This may need flush_rewrite_rules() to be called before...
     * @since 1.0.0
     */
	public function rewrite_rule() {

	    add_rewrite_rule( '^oai/?([^/]*)', 'index.php?wpoaipmh=true&$matches[1]', 'top' );

	}

    /**
     * Sets the url variables to be used by the protocol and plugin.
     * wpoaipmh: needs to be present for the plugin to know this is a oai-pmh request to be handled by the plugin.
     * verb, identifier, metadataPrefix, from, until, set -> OAI-PMH request variables
     * @since 1.0.0
     */
    public function url_vars() {

        add_rewrite_tag( '%wpoaipmh%', '([^&]+)' );
        add_rewrite_tag( '%verb%', '([^&]+)' );
        add_rewrite_tag( '%identifier%', '([^&]+)' );
        add_rewrite_tag( '%metadataPrefix%', '([^&]+)' );
        add_rewrite_tag( '%from%', '([^&]+)' );
        add_rewrite_tag( '%until%', '([^&]+)' );
        add_rewrite_tag( '%set%', '([^&]+)' );
        add_rewrite_tag( '%resumptionToken%', '([^&]+)' );

    }

    /**
     * Intercept the $wpquery global so that if it contains 'wpoaipmh' as a query_var the oai-pmh template is returned.
     *
     * @since 1.0.0
     */
    public function wpoaipmh_template($template){

        global $wp_query;
        if( isset( $wp_query->query_vars['wpoaipmh'] ) ) {
            return dirname(__FILE__) . '/templates/wpoaipmh_template.php';
        }
        return $template;

    }

}
