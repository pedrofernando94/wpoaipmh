<?php  

/**
* ListMetadataFormats TEMPLATE
* ----------------------------
* Our implementation of the protocol only allows oai_dc format. Fill in with your Repo informations...
*
*/
?>
<ListMetadataFormats>
	<metadataFormat>
        <metadataPrefix>oai_dc</metadataPrefix>
        <schema>http://www.openarchives.org/OAI/2.0/oai_dc.xsd</schema>
        <metadataNamespace>http://www.openarchives.org/OAI/2.0/oai_dc/</metadataNamespace>
    </metadataFormat>
    <metadataFormat>
        <metadataPrefix>mods</metadataPrefix>
        <schema>http://www.loc.gov/standards/mods/v3/mods-3-5.xsd</schema>
        <metadataNamespace>http://www.loc.gov/mods/v3</metadataNamespace>
    </metadataFormat>
</ListMetadataFormats>