<?php 

/**
* Identify TEMPLATE
* -----------------
* Fill in with your Repository informations...
*
*/

?>
<Identify>
	<repositoryName><?= get_bloginfo( 'name' ) ?></repositoryName>
	<baseURL><?= get_site_url() ?>/oai/</baseURL>
	<protocolVersion>2.0</protocolVersion>
	<adminEmail><?= get_bloginfo( 'admin_email' ) ?></adminEmail>
	<earliestDatestamp>1900-01-01</earliestDatestamp>
	<deletedRecord>no</deletedRecord>
	<granularity>YYYY-MM-DD</granularity>		
</Identify>
