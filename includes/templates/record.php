<?php 

/**
* Record TEMPLATE
* ---------------
*
*/

//$datestamp = 'yyyy-mm-dd';
$datestamp = date('Y-m-d', strtotime($record['post']->post_date));
// Get WP_POST OBJECT for the id... to access: apply_filters( 'the_content', $post->post_content ); instead of $post->post_content
$header = "<header>";
$header .= "<identifier>" . $record['post']->ID . "</identifier>";
$header .= "<datestamp>" . $datestamp . "</datestamp>";
//$header .= $line_setSpec1;
//$header .= $line_setSpec2;
$header .= "</header>";

if($this->verb == "ListIdentifiers") {

	echo($header);

} else { ?>
<record>
	<?= $header ?>
	<metadata>
	    <?= $record['metadata_xml'] ?>
    </metadata>
</record>
<?php } ?>