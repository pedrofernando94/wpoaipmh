<?php  

/**
* ListRecords TEMPLATE
* --------------------
* Used for ListRecords and ListIdentifiers (in $this->verb)
* noRecordsMatch Error is triggered here.
*
*/

use WPOAIPMH\Repository;

$requester = new  \WPOAIPMH\Repository\Requester();

$set             = $this->arguments["set"];
$from            = $this->arguments["from"];
$until           = $this->arguments["until"];
$resumptionToken = $this->arguments["resumptionToken"];
$metadataPrefix  = $this->arguments["metadataPrefix"];

$records = $requester->listRecords($from, $until, $set, $resumptionToken, $metadataPrefix);

if(!empty($records)) { 

	/* <ListRecords> ou <ListIdentifiers> */
	echo('<' . $this->verb . '>');

	/* Les records (ou identifiers)... */
	foreach ($records as $record) include('record.php');
	
	/* </ListRecords> ou </ListIdentifiers> */
	echo('</' . $this->verb . '>');

}  else { 

	echo('<error code="noRecordsMatch"></error>');

}

?>