<?php

/**
 * Fired during plugin activation
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/includes
 * @author     Pedro Monteiro <29683@ufp.edu.pt>
 */
class Wpoaipmh_Activator {

	/**
	 * Called when plugin is activated.
     *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
