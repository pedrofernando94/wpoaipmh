<?php

/**
 * Fired during plugin deactivation
 *
 * @link       po-ex.net
 * @since      1.0.0
 *
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wpoaipmh
 * @subpackage Wpoaipmh/includes
 * @author     Pedro Monteiro <29683@ufp.edu.pt>
 */
class Wpoaipmh_Deactivator {

	/**
	 * Called when plugin is deactivated.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
