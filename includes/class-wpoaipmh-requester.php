<?php

namespace WPOAIPMH\Repository;

class Requester
{

    public function __construct() {

    }

    public function listRecords($from = '', $until = '', $set = '', $resumptionToken = '', $metadataPrefix = '') {

        $result = [];

        /* TODO: Exception when the SET is set and not null but is not a valid set.
         *       if that happens it should return empty response. Maybe Error?
         *       EX: if($set != '' &&  NOT VALID SET) return [];
         */

        /* THIS REQUEST RETURNS ALL POTENTIALLY AVAILABLE RESSOURCES */
        /* Adapt this request with your needs ... */
        $args = array(
            'posts_per_page'   => 500, //1000 should be ok without a resumption token
            'offset'           => 0,
            'post_type'        => array('post'),
            'post_status'      => 'publish',
            'meta_key'         => '',
            'order'            => 'ASC',
            'orderby'          => 'ID',
            'date_query'       => array( 'after' => strtotime($from),
                                         'before' => strtotime($until),
                                         'inclusive' => true),

            'meta_key'         => '_wpoaipmh_record_oai_activated',
            'meta_value'       => 'on'
        );

        $query = new \WP_Query($args);
        $posts = $query->get_posts();

        /* from and until dates must be in the format: 2015-01-31 */
        // Should transform into the date format: 20150131 only for database comparison:
        //if($from != '')  $from_cmp  = str_replace('-', '', $from);
        //if($until != '') $until_cmp = str_replace('-', '', $until);

        foreach ($posts as $post) {
            $tobeincluded = true;

            //if($set != '' && DOES NOT BELONG TO SET) $tobeincluded = false;
            //$date = $post->post_date;
            //if($from_cmp != '' && $date < $from_cmp) $tobeincluded = false;
            //if($until_cmp != '' && $date > $until_cmp) $tobeincluded = false;

            /* If the request corresponds to what is asked, include it. */
            if($tobeincluded) {
                array_push($result, array(
                    'post' =>  $post,
                    'metadata_xml' => $this->get_record_metadata_xml($post->ID, $metadataPrefix)
                    )
                );
            }
        }

        return $result;

    }

    /* Returns an array with all available SET ids... */
    public function getSets() {

        return array();

    }

    /* Returns an array that contains the collection and sub-collection ids that are oai-pmh sets */
    public function listSets($resumptionToken = '') {

        $sets = $this->getSets();
        foreach ($sets as $pair) {
            $col_id   = $pair[0];
            $sscol_id = $pair[1];
            $IDENTIFICATION = (empty($sscol_id)) ? $col_id : $sscol_id;
            include( __DIR__ . '/templates/set.php' );
        }

    }

    public function get_record_metadata_xml($post_id, $metadataPrefix) {
        if($metadataPrefix == 'mods') {
            $metadata_xml = $this->get_record_metadata_mods($post_id);
        }
        else {
            $metadata_xml = $this->get_record_metadata_dc($post_id);
        }
        //$other_metadata_xml = apply_filters( 'wpoaipmh_metadata_xml', $post_id, $metadataPrefix ); // IF the return equals $post_id nothing was done.
        //return ( empty($other_metadata_xml)  || $other_metadata_xml == $post_id ) ? $metadata_xml : $other_metadata_xml;
        return $metadata_xml;
    }

    public function get_record_metadata_dc($post_id) {

        $post = get_post($post_id);
        $slug = '_wpoaipmh';
        //$_record_oai_activated = get_post_meta( $post_id, $slug . '_record_oai_activated', true );
        $_record_title = get_post_meta( $post_id, $slug . '_record_title', true );
        $_record_description = get_post_meta( $post_id, $slug . '_record_description', true );
        $_record_year_original_publication = get_post_meta( $post_id, $slug . '_record_year_original_publication', true );
        $_record_work_languages = json_decode( get_post_meta( $post_id, $slug . '_record_work_languages', true ), true );
        $_record_technology = get_post_meta( $post_id, $slug . '_record_technology', true );
        $_record_publication_types = json_decode( get_post_meta( $post_id, $slug . '_record_publication_types', true ), true );
        $_record_complementary_publication_types = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_publication_types', true ), true );
        $_record_procedural_modalities = json_decode( get_post_meta( $post_id, $slug . '_record_procedural_modalities', true ), true );
        $_record_complementary_procedural_modalities = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_procedural_modalities', true ), true );
        $_record_mechanisms = json_decode( get_post_meta( $post_id, $slug . '_record_mechanisms', true ), true );
        $_record_complementary_mechanisms = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_mechanisms', true ), true );
        $_record_formats = json_decode( get_post_meta( $post_id, $slug . '_record_formats', true ), true );
        $_record_complementary_formats = json_decode( get_post_meta( $post_id, $slug . '_record_complementary_formats', true ), true );
        $_record_literary_qualities = json_decode( get_post_meta( $post_id, $slug . '_record_literary_qualities', true ), true );
        $_record_creators = json_decode( get_post_meta( $post_id, $slug . '_creators', true ), true );
        $_record_publishers = json_decode( get_post_meta( $post_id, $slug . '_publishers', true ), true );
        $_record_source_name = get_post_meta( $post_id, $slug . '_record_source_name', true );
        $_record_source_authors = get_post_meta( $post_id, $slug . '_record_source_authors', true );

        $metadata_xml = '
        <oai_dc:dc xmlns:dc="http://purl.org/dc/elements/1.1/"
           xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
            <dc:identifier>' . cleanforxml(get_permalink($post_id)) . '</dc:identifier>
            <dc:title>' . cleanforxml($_record_title) . '</dc:title>
            <dc:description>' . cleanforxml($_record_description) . '</dc:description>
            <dc:relation>' . cleanforxml(get_permalink($post_id)) . '</dc:relation>
            <dc:date>' . cleanforxml($_record_year_original_publication) . '</dc:date>
            <dc:format>' . cleanforxml($_record_technology) . '</dc:format>';
        
        # Creators
        if($_record_creators) {
            foreach ($_record_creators as $creator) {
                $metadata_xml .= '<dc:creator>' . cleanforxml($creator["name"]) . '</dc:creator>';
            }
        }
        # Publishers
        if($_record_publishers) {
            foreach ($_record_publishers as $publisher) {
                $metadata_xml .= '<dc:publisher>' . cleanforxml($publisher["name"]) . '</dc:publisher>';
            }
        }
        # Languages
        if($_record_work_languages) {
            foreach ($_record_work_languages as $work_language) {
                $metadata_xml .= '<dc:language>' . cleanforxml($work_language) . '</dc:language>';
            }
        }
        # Publication Types
        if($_record_publication_types) {
            foreach ($_record_publication_types as $publication_type) {
                $metadata_xml .= '<dc:format>' . cleanforxml($publication_type) . '</dc:format>';
            }
        }
        if($_record_complementary_publication_types) {
            foreach ($_record_complementary_publication_types as $complementary_publication_type) {
                $metadata_xml .= '<dc:format>' . cleanforxml($complementary_publication_type) . '</dc:format>';
            }
        }
        # Mechanisms
        if($_record_mechanisms) {
            foreach ($_record_mechanisms as $mechanism) {
                $metadata_xml .= '<dc:format>' . cleanforxml($mechanism) . '</dc:format>';
            }
        }
        if($_record_complementary_mechanisms) {
            foreach ($_record_complementary_mechanisms as $complementary_mechanism) {
                $metadata_xml .= '<dc:format>' . cleanforxml($complementary_mechanism) . '</dc:format>';
            }
        }
        # Formats
        if($_record_formats) {
            foreach ($_record_formats as $format) {
                $metadata_xml .= '<dc:format>' . cleanforxml($format) . '</dc:format>';
            }
        }
        if($_record_complementary_formats) {
            foreach ($_record_complementary_formats as $complementary_format) {
                $metadata_xml .= '<dc:format>' . cleanforxml($complementary_format) . '</dc:format>';
            }
        }
        # Procedural Modalities
        if($_record_procedural_modalities) {
            foreach ($_record_procedural_modalities as $procedural_modality) {
                $metadata_xml .= '<dc:type>' . cleanforxml($procedural_modality) . '</dc:type>';
            }
        }
        if($_record_complementary_procedural_modalities) {
            foreach ($_record_complementary_procedural_modalities as $complementary_procedural_modality) {
                $metadata_xml .= '<dc:type>' . cleanforxml($complementary_procedural_modality) . '</dc:type>';
            }
        }
        # Literary Qualities
        if($_record_literary_qualities) {
            foreach ($_record_literary_qualities as $literary_quality) {
                $metadata_xml .= '<dc:type>' . cleanforxml($literary_quality) . '</dc:type>';
            }
        }

        $metadata_xml .= '</oai_dc:dc>';

        return $metadata_xml;
    }

    public function get_record_metadata_mods($post_id)
    {

        $post = get_post($post_id);
        $slug = '_wpoaipmh';
        //$_record_oai_activated = get_post_meta( $post_id, $slug . '_record_oai_activated', true );
        $_record_title = get_post_meta($post_id, $slug . '_record_title', true);
        $_record_description = get_post_meta($post_id, $slug . '_record_description', true);
        $_record_year_original_publication = get_post_meta($post_id, $slug . '_record_year_original_publication', true);
        $_record_work_languages = json_decode(get_post_meta($post_id, $slug . '_record_work_languages', true), true);
        $_record_technology = get_post_meta($post_id, $slug . '_record_technology', true);
        $_record_publication_types = json_decode(get_post_meta($post_id, $slug . '_record_publication_types', true), true);
        $_record_complementary_publication_types = json_decode(get_post_meta($post_id, $slug . '_record_complementary_publication_types', true), true);
        $_record_procedural_modalities = json_decode(get_post_meta($post_id, $slug . '_record_procedural_modalities', true), true);
        $_record_complementary_procedural_modalities = json_decode(get_post_meta($post_id, $slug . '_record_complementary_procedural_modalities', true), true);
        $_record_mechanisms = json_decode(get_post_meta($post_id, $slug . '_record_mechanisms', true), true);
        $_record_complementary_mechanisms = json_decode(get_post_meta($post_id, $slug . '_record_complementary_mechanisms', true), true);
        $_record_formats = json_decode(get_post_meta($post_id, $slug . '_record_formats', true), true);
        $_record_complementary_formats = json_decode(get_post_meta($post_id, $slug . '_record_complementary_formats', true), true);
        $_record_literary_qualities = json_decode(get_post_meta($post_id, $slug . '_record_literary_qualities', true), true);
        $_record_creators = json_decode(get_post_meta($post_id, $slug . '_creators', true), true);
        $_record_publishers = json_decode(get_post_meta($post_id, $slug . '_publishers', true), true);
        $_record_source_name = get_post_meta($post_id, $slug . '_record_source_name', true);
        $_record_source_authors = get_post_meta($post_id, $slug . '_record_source_authors', true);

        $metadata_xml = '<mods xmlns="http://www.loc.gov/mods/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:cell="http://cellproject.net/schemas/cell-mods-extension">';
        // Identifier
        $metadata_xml .= '<identifier type="uri">' . cleanforxml(get_permalink($post_id)) . '</identifier>';
        // Source
        $metadata_xml .= '<recordContentSource authorityURI="http://cellproject.net/authorities/source-database">' . cleanforxml(get_bloginfo("name")) . '</recordContentSource>';
        $metadata_xml .= '<recordCreationDate encoding="iso8601">' . get_the_date("Y-m-d\TH:m:s\Z", $post_id) . '</recordCreationDate>';
        $metadata_xml .= '<recordChangeDate encoding="iso8601">' . get_the_modified_time("Y-m-d\TH:m:s\Z", $post_id) . '</recordChangeDate>';
        // Title
        $metadata_xml .= '<titleInfo><title>' . cleanforxml($_record_title) . '</title></titleInfo>';
        // Description
        if($_record_description) {
            $metadata_xml .= '<abstract>' . $_record_description . '</abstract>';
        }
        // Url
        $metadata_xml .= '<location><url>' . cleanforxml(get_permalink($post_id)) . '</url></location>';

        $metadata_xml .= '<originInfo>';
        // Year of Original Publication
        if($_record_year_original_publication) {
            $metadata_xml .= '<dateIssued encoding="w3cdtf">' . cleanforxml($_record_year_original_publication) . '</dateIssued>';
        }
        // Publishers
        if($_record_publishers) {
            foreach ($_record_publishers as $publisher) {
                $metadata_xml .= '<publisher>' . cleanforxml($publisher["name"]) . '</publisher>';
            }
        }
        $metadata_xml .= '</originInfo>';

        // Creators Name
        if($_record_creators) {
            $metadata_xml .= '<name authority="local">';
            foreach ($_record_creators as $creator) {
                $metadata_xml .= '<namePart>' . cleanforxml($creator["name"]) . '</namePart>';
            }
            $metadata_xml .= '</name>';
        }

        // Languages
        $metadata_xml .= '<language>';
        if($_record_work_languages) {
            foreach ($_record_work_languages as $language) {
                $metadata_xml .= '<languageTerm authority="rfc3066">' . cleanforxml($language) . '</languageTerm>';
            }
        }
        $metadata_xml .= '</language>';

        $metadata_xml .= '<physicalDescription>';
        // Technology
        if($_record_technology) {
            $metadata_xml .= '<form type="technology" authorityURI="http://cellproject.net/authorities/technology" valueURI="' . cleanforxml($_record_technology) . '">' . cleanforxml($_record_technology) . '</form>';
        }
        // Publication Types
        if($_record_publication_types) {
            foreach ($_record_publication_types as $publication_type) {
                $metadata_xml .= '<form type="publication-type" authorityURI="http://cellproject.net/authorities/publication-type" valueURI="' . cleanforxml($publication_type) . '">' . cleanforxml($publication_type) . '</form>';
            }
        }
        // Complementary Publication Types
        if($_record_complementary_publication_types) {
            foreach ($_record_complementary_publication_types as $complementary_publication_type) {
                $metadata_xml .= '<form type="publication-type">' . $complementary_publication_type . '</form>';
            }
        }
        // Mechanisms
        if($_record_mechanisms) {
            foreach ($_record_mechanisms as $mechanism) {
                $metadata_xml .= '<form type="mechanism" authorityURI="http://cellproject.net/authorities/mechanism" valueURI="' . cleanforxml($mechanism) . '">' . cleanforxml($mechanism) . '</form>';
            }
        }
        // Complementary Mechanisms
        if($_record_complementary_mechanisms) {
            foreach ($_record_complementary_mechanisms as $complementary_mechanism) {
                $metadata_xml .= '<form type="mechanism">' . cleanforxml($complementary_mechanism) . '</form>';
            }
        }
        // Formats
        if($_record_formats) {
            foreach ($_record_formats as $format) {
                $metadata_xml .= '<form type="format" authorityURI="http://cellproject.net/authorities/format" valueURI="' . cleanforxml($format) . '">' . cleanforxml($format) . '</form>';
            }
        }
        // Complementary Formats
        if($_record_complementary_formats) {
            foreach ($_record_complementary_formats as $complementary_format) {
                $metadata_xml .= '<form type="format">' . cleanforxml($complementary_format) . '</form>';
            }
        }

        $metadata_xml .= '</physicalDescription>';

        // Procedural Modalities
        if($_record_procedural_modalities) {
            foreach ($_record_procedural_modalities as $procedural_modality) {
                $metadata_xml .= '<genre type="procedural-modality" authorityURI="http://cellproject.net/authorities/procedural-modality" valueURI="' . $procedural_modality . '">' . $procedural_modality . '</genre>';
            }
        }
        // Complementary Procedural Modalities
        if($_record_complementary_procedural_modalities) {
            foreach ($_record_complementary_procedural_modalities as $complementary_procedural_modality) {
                $metadata_xml .= '<genre type="procedural-modality">' . $complementary_procedural_modality . '</genre>';
            }
        }
        // Literary Qualities
        if($_record_literary_qualities) {
            foreach ($_record_literary_qualities as $literary_quality) {
                $metadata_xml .= '<genre type="literary-quality">' . $literary_quality . '</genre>';
            }
        }

        // CELL MODS Custom Fields
        $metadata_xml .= '<cell:recordInfo>';
        if($_record_creators) {
            $metadata_xml .= '<name>';
            foreach ($_record_creators as $creator) {
                $metadata_xml .= '<namePart>' . cleanforxml($creator["name"]) . '</namePart>';
            }
            $metadata_xml .= '</name>';
        }
        $metadata_xml .= '</cell:recordInfo>';

        $metadata_xml .= '</mods>';

        return $metadata_xml;
    }

}