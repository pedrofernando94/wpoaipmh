<?php

namespace WPOAIPMH\Repository;

class ResponseBuilder
{
    
    protected $verb;
    protected $arguments;
    protected $errors;

    public $keys = [ 'identifier', 'metadataPrefix', 'from', 'until', 'set', 'resumptionToken' ];

    public function __construct() {

        $this->verb = get_query_var( 'verb' ) != '' ? get_query_var( 'verb' ) : 'Identify';
        $this->errors = [];

        foreach ($this->keys as $key) {
            $this->arguments[$key] = get_query_var($key);
        }

        $this->analyse_verb();
        if(empty($this->errors)) {
            $this->analyse_request();
        }

    }

    /*
     * Error Handling function for the verb only.
     */
    private function analyse_verb() {

        $verbs = ["GetRecord", "Identify", "ListIdentifiers", "ListMetadataFormats", "ListRecords", "ListSets"];
        if ( !in_array($this->verb, $verbs) ) {
            $this->errors["badVerb"] = "Bad Verb";
        }

    }

    /*
     * Main Error Handling Function: this calls all other error handling functions with some logic for when to do so.
     */
    private function analyse_request() {

        /* Error : badVerb -> generate procedure */
        $this->analyze_required_params();

        /* Error : badArgument -> for the verb passed */
        $this->analyze_illegal_params();

        /* Error : cannotDisseminateFormat -> for GetRecord, ListIdentifiers, ListRecords */
        if (in_array($this->verb, ['GetRecord', 'ListIdentifiers', 'ListRecords']))
            $this->analyze_metadataprefix();

        /* Error : idDoesNotExist -> for GetRecord, ListMetadataFormats */
        if (in_array($this->verb, ['GetRecord', 'ListMetadataFormats']))
            $this->analyze_identifier();

        /* Error : noMetadataFormats -> Only For ListMetadataFormats */
        /* Only uses MetadataFormat oai_dc for now, so it does not send out this error */

        /* Error : noSetHierarchy -> Does not support sets */
        if ($this->verb == 'ListSets')
            $this->errors["noSetHierarchy"] = "This repository does not support sets.";


        /* Error : noRecordsMatch -> triggered at record.php template level */
        /* Error : badResumptionToken -> resumptionToken not implemented so far */

    }

    /*
     * Error Handling Function: makes sure that required verb arguments were passed for the requested verb.
     */
    private function analyze_required_params() {

        $REQUIREMENTS = array(
            'GetRecord' => ['identifier', 'metadataPrefix'],
            'Identify' => [],
            'ListIdentifiers' => ['metadataPrefix'],
            'ListMetadataFormats' => [],
            'ListRecords' => ['metadataPrefix'],
            'ListSets' => []
        );

        $required_args= $REQUIREMENTS[$this->verb];
        //if ($this->arguments['resumptionToken'] == '') { // Don't support resumptionToken
            foreach ($required_args as $param) {
                if ($this->arguments[$param] == '') {
                    $this->errors["badArgument"] = "One or more arguments is missing";
                    return;
                }
            }
        //}

    }

    /*
     * Error Handling Function: makes sure that no invalid arguments were passed for the requested verb.
     */
    private function analyze_illegal_params() {

        $LEGAL = array(
            'GetRecord' => ['identifier', 'metadataPrefix'],
            'Identify' => [],
            'ListIdentifiers' => ['from', 'until', 'metadataPrefix', 'set', 'resumptionToken'],
            'ListMetadataFormats' => ['identifier'],
            'ListRecords' => ['from', 'until', 'metadataPrefix', 'set', 'resumptionToken'],
            'ListSets' => ['resumptionToken']
        );

        $legal_params = $LEGAL[$this->verb];
        foreach ( $this->keys as $key ) {
            if ( get_query_var($key) != '' && !in_array( $key, $legal_params ) ) {
                $this->errors["badArgument"] = "Illegal Argument";
                return;
            }
        }

    }

    /*
     * Error Handling function: makes sure that the metadataPrefix requested is supported by the repository.
     * Also this function implements a WP filter hook so that other plugins can extend the metadata capability.
     */
    private function analyze_metadataprefix() {

        $metadata_prefixes = array();
        array_push($metadata_prefixes, 'oai_dc');
        array_push($metadata_prefixes, 'mods');
        $metadata_prefixes = apply_filters( 'wpoaipmh_add_metadata_prefix', $metadata_prefixes );

        foreach ( $metadata_prefixes as $metadata_prefix ) {
            if ( $this->arguments['metadataPrefix'] == $metadata_prefix ) {
               return; // If it returns from the function all is well...
            }
        }
        $this->errors["cannotDisseminateFormat"] = "The repository does not support the metadataPrefix.";

    }

    /*
     * Error Handling function: makes sure the identifier requested is valid.
     */
    private function analyze_identifier() {

        $id = $this->arguments['identifier'];
        /* Don't return error if the identifier is not specified in the case of ListMetadataFormats
         * the identifier is optional...
         * TODO: This can be made better by checking the verb...
         */
        if ($id == "") return;

        // TODO: TEST TO CHECK IF WE SHOULD GIVE OUT A ERROR: $idDoesNotExist = true;
        $idDoesNotExist = false;

        if ($idDoesNotExist)
            $this->errors["idDoesNotExist"] = "Requested identifier does not exist.";

    }

    /*
     * The function called after this class is constructed...
     * It formats and prints the response OAI-PMH XML to the browser.
     */
    public function render() {

        Header('Content-type: text/xml');
        header("Access-Control-Allow-Origin: *");
        $this->print_header();
        $this->print_request();
        if (empty($this->errors)) $this->print_content();
        else $this->print_errors();
        $this->print_footer();
        die();

    }

    /*
     * Prints the XML for the OAI-PMH header.
     */
    private function print_header() {

        $now = date('Y-m-d\TH:i:s\Z');
        print('<?xml version="1.0" encoding="UTF-8"?>');
        print('<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">');
        print('<responseDate>' . $now . '</responseDate>');

    }

    /*
     * Prints the required request tag for the oai-pmh xml.
     */
    private function print_request() {

        $request = '<request verb="' . $this->verb . '"';
        foreach ($this->arguments as $key => $value) {
            if ($value != '') $request .= (' ' . $key . '="' . $value . '"');
        }
        $url = get_site_url() . '/oai';
        $request .= '>' . $url . '</request>';
        print($request);

    }

    /*
     * This function prints the template regarding the verb submitted.
     */
    private function print_content() {

        include(__DIR__ . '/templates/' . $this->verb . '.php');

    }

    /*
     * If there where errors this function prints them.
     */
    private function print_errors() {

        foreach ($this->errors as $code => $message) {
            print('<error code="' . $code . '">' . $message . '</error>');
        }

    }

    /*
     * Simply prints out the end of OAI-PMH xml file.
     */
    private function print_footer() {

        print('</OAI-PMH>');

    }

}