<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              po-ex.net
 * @since             1.0.0
 * @package           Wpoaipmh
 *
 * @wordpress-plugin
 * Plugin Name:       Wordpress OAI-PMH
 * Plugin URI:        ufp.pt
 * Description:       Wordpress plugin that implements a OAI-PMH Repository.
 * Version:           1.0.0
 * Author:            Pedro Monteiro
 * Author URI:        po-ex.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpoaipmh
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WPOAIPMH', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wpoaipmh-activator.php
 */
function activate_wpoaipmh() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wpoaipmh-activator.php';
	Wpoaipmh_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wpoaipmh-deactivator.php
 */
function deactivate_wpoaipmh() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wpoaipmh-deactivator.php';
	Wpoaipmh_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wpoaipmh' );
register_deactivation_hook( __FILE__, 'deactivate_wpoaipmh' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wpoaipmh.php';

/* The ResponseBuilder Class builds the OAI-PMH response to a request */
require plugin_dir_path(__FILE__) . 'includes/class-wpoaipmh-responsebuilder.php';

/* The Requester Class makes queries on the WordPress Database */
require plugin_dir_path(__FILE__) . 'includes/class-wpoaipmh-requester.php';


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wpoaipmh() {

	$plugin = new Wpoaipmh();
	$plugin->run();

}
run_wpoaipmh();

/* Utility function for XML cleaning of Strings */
function cleanforxml($string)
{
    $string = str_replace(array('&rsquo;'), array("'"), $string);
    $string = str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
    return $string;
}




